package com.example.bookingapiservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
public class BookingApiServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(BookingApiServiceApplication.class, args);
    }

}
