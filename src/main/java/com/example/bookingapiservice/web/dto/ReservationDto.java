package com.example.bookingapiservice.web.dto;

import com.example.bookingapiservice.model.entity.ReservationEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReservationDto {
    private String title;
    private String startDttm;
    private String endDttm;
    private String memo;
    private String resourceId;

    public ReservationEntity toEntity() {
        return ReservationEntity.builder()
                .title(title)
                .startDttm(startDttm)
                .endDttm(endDttm)
                .memo(memo)
                .resourceId(resourceId)
                .build();
    }
}
