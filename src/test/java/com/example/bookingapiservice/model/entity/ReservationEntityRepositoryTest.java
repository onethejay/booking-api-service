package com.example.bookingapiservice.model.entity;

import com.example.bookingapiservice.web.dto.ReservationDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class ReservationEntityRepositoryTest {

    @Autowired
    ReservationRepository reservationRepository;

    @DisplayName("1. 예약 테이블 조회 테스트")
    @Test
    void test_1(){
        System.out.println(reservationRepository.findAll());
    }

    @DisplayName("2. 예약 테이블 저장 테스트")
    @Test
    @Transactional
    void test_2(){
        ReservationDto dto = ReservationDto.builder()
                .title("테스트 예약2")
                .startDttm("20220314090000")
                .endDttm("20220314115959")
                .memo("테스트 메모2")
                .resourceId("2")
                .build();

        ReservationEntity saveEntity = reservationRepository.save(dto.toEntity());

        assertThat(saveEntity.getTitle()).isEqualTo(dto.getTitle());
    }

}