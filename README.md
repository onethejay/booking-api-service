# 예약 사이트 백엔드 서비스 

### 프로젝트 설명

예약 사이트 백엔드 서비스를 실습해보기 위한 프로젝트입니다.

### 프로젝트 구성
Backend
* Springboot Web
* JPA
* QueryDSL

[Frontend](https://github.com/onethejay)
* Vue.js
* Vue-router
* Vuex